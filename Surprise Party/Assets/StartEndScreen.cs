﻿using PixelCrushers.DialogueSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StartEndScreen : MonoBehaviour
{
    public GameObject trigger;

    private void StartConvo()
    {
        trigger.GetComponent<DialogueSystemTrigger>().enabled = true;
    }


    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartConvo();
        }
    }

}
