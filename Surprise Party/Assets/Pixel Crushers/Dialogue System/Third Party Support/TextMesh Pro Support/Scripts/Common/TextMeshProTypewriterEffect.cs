﻿#if TMP_PRESENT
// Copyright © Pixel Crushers. All rights reserved.

using UnityEngine;

namespace PixelCrushers.DialogueSystem.TextMeshProSupport
{

    /// <summary>
    /// This points to the new TMP Typewriter Effect in the Dialogue System core product.
    /// </summary>
    [AddComponentMenu("")]
    [DisallowMultipleComponent]
    public class TextMeshProTypewriterEffect : PixelCrushers.DialogueSystem.Wrappers.TextMeshProTypewriterEffect
    {
        public bool IsPlaying { get; internal set; }
    }

}
#endif
