﻿using TMPro;
using UnityEngine;

namespace PixelCrushers.DialogueSystem.TextMeshProSupport
{

    /// <summary>
    /// Localizes the text content of a TextMeshPro and/or TextMeshProUGUI component.
    /// </summary>
    [AddComponentMenu("Pixel Crushers/Dialogue System/Third Party/TextMesh Pro/Localize TextMesh Pro")]
    public class LocalizeTextMeshPro : LocalizeUI
    {

        private TMPro.TextMeshPro m_textMeshPro;
        public TMPro.TextMeshPro textMeshPro
        {
            get { return m_textMeshPro; }
            set { m_textMeshPro = value; }
        }
        private bool m_lookedForTMPNotUGUI = false;
        private bool m_lookedForTMPUGUI = false;
        private TextMeshProUGUI textMeshProUGUI;

        public override void UpdateText()
        {
            if (!started) return;
            var textTable = this.textTable;
            var language = UILocalizationManager.instance.currentLanguage;
            if (string.IsNullOrEmpty(language)) return;

            // Skip if no language set:
            if (textTable == null)
            {
                Debug.LogWarning("No localized text table is assigned to " + name + " or a UI Localized Manager component.", this);
                return;
            }

            if (!textTable.HasLanguage(language))
            {
                Debug.LogWarning("Text table " + textTable.name + " does not have a language '" + language + "'.", textTable);
                return;
            }

            // UGUI version:
            if (!m_lookedForTMPUGUI)
            {
                m_lookedForTMPUGUI = true;
                textMeshProUGUI = GetComponent<TMPro.TextMeshProUGUI>();
            }
            if (textMeshProUGUI != null)
            {
                if (string.IsNullOrEmpty(fieldName))
                {
                    fieldName = (textMeshProUGUI != null) ? textMeshProUGUI.text : string.Empty;
                }
                if (!textTable.HasField(fieldName))
                {
                    Debug.LogWarning("Text table " + textTable.name + " does not have a field '" + fieldName + "'.", textTable);
                }
                else
                {
                    textMeshProUGUI.text = GetLocalizedText(fieldName);
                }
            }

            // Non-UGUI version:
            if (!m_lookedForTMPNotUGUI)
            {
                m_lookedForTMPNotUGUI = true;
                textMeshPro = GetComponent<TMPro.TextMeshPro>();
            }
            if (textMeshPro != null)
            {
                if (string.IsNullOrEmpty(fieldName))
                {
                    fieldName = (textMeshPro != null) ? textMeshPro.text : string.Empty;
                }
                if (!textTable.HasField(fieldName))
                {
                    Debug.LogWarning("Text table " + textTable.name + " does not have a field '" + fieldName + "'.", textTable);
                }
                else
                {
                    textMeshPro.text = GetLocalizedText(fieldName);
                }
            }
        }
    }
}