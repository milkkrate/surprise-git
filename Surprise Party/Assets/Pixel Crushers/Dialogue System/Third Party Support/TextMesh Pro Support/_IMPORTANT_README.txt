/*
This folder contains the *LEGACY* support package for TextMesh Pro.
It exists to support older projects that don't use the Standard Dialogue UI.

DO NOT USE THIS FOR NEW PROJECTS!

If you're creating a new project, use the Standard Dialogue UI system and
simply enable TextMesh Pro support for it:

https://www.pixelcrushers.com/dialogue_system/manual2x/html/dialogue_u_is.html#dialogueUITMProSupport
*/