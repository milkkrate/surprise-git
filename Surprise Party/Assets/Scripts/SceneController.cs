﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public Animator anim;

    private void Awake()
    {
        FadeOut();
    }

    public void StartGame()
    {
        FadeIn();
        StartCoroutine(Load());
    }

    //transition to black

    public void FadeIn()
    {
        anim.SetTrigger("FadeIn");
    }

    //transition from black to alpha
    public void FadeOut()
    {
        anim.SetTrigger("FadeOut");
    }


    //transition to black then back to alpha
    public void FadeInAndOut()
    {
        anim.SetTrigger("FadeInAndOut");
    }

    IEnumerator Load()
    {
        yield return new WaitForSeconds(1.5f);

        SceneManager.LoadScene(1);
    }

    public void LoadWin()
    {
        FadeOut();
        SceneManager.LoadScene("YouWin");
    }
}
