﻿using RotaryHeart.Lib;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelCrushers.DialogueSystem;
public class HidingSpotManager : MonoBehaviour
{
   
    public GameObject selectedNPC;
    public HidingSpotSlot selectedHidingSpot;
    public List<HidingSpotSlot> hidingSpots;

    public bool CheckIfSpotIsEmpty(HidingSpotSlot spotToCheck)
    {
        if (spotToCheck.npcInLocation != null)
            return true;
        else
            return false;
    }

    public void MoveNPCToHidingSpot()
    {
        if (selectedHidingSpot != null)
        {
            if (selectedNPC.GetComponent<MoveNPCToHSpot>().currentSpot != null)
            {
                selectedNPC.GetComponent<MoveNPCToHSpot>().currentSpot.GetComponent<HidingSpotSlot>().npcInLocation = null;
                selectedNPC.GetComponent<MoveNPCToHSpot>().currentSpot.GetComponent<Usable>().enabled = true;
            }



            selectedNPC.GetComponent<MoveNPCToHSpot>().currentSpot = selectedHidingSpot;
            
            selectedNPC.transform.position = selectedHidingSpot.hidingSpotLocation.transform.position;
            selectedNPC.transform.rotation = selectedHidingSpot.hidingSpotLocation.transform.rotation;

            selectedHidingSpot.npcInLocation = selectedNPC;
            selectedHidingSpot.GetComponent<Usable>().enabled = false;

            

            //previousHidingSpot.npcInLocation = null;

            selectedHidingSpot = null;
            DialogueLua.SetVariable("hasHidingSpotLoaded", false);
            DialogueLua.SetVariable("currentHidingSpot", "nowhere");
        }


    }

    public void ChangeHeldHidingSpot(HidingSpotSlot newHidingSpot)
    {
        selectedHidingSpot = newHidingSpot;
    }

    public void ChangeHeldNPC(GameObject newNPC)
    {
        selectedNPC = newNPC;
    }

    public void LoadHidingSpotIntoDialogue()
    {
        if (selectedHidingSpot != null)
        {
            DialogueLua.SetVariable("currentHidingSpot", selectedHidingSpot.name);
            DialogueLua.SetVariable("hasHidingSpotLoaded", true);
        }


    }

}