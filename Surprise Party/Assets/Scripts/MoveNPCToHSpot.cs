﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveNPCToHSpot : MonoBehaviour
{
    public HidingSpotSlot currentSpot;

    public void MoveToHidingSpot (GameObject hidingSpotLocation)
    {
        if (hidingSpotLocation != null)
        {
            this.transform.parent = hidingSpotLocation.transform;
            transform.position = hidingSpotLocation.transform.position;
            transform.rotation = hidingSpotLocation.transform.rotation;
        }
        return;
    }

}
