﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public float timeRemaining = 600;
    public bool timerIsRunning = true;
    public TextMeshProUGUI timeText;
    
    SceneController sceneController;

    private void Awake()
    {
        sceneController = gameObject.GetComponent<SceneController>();
    }

    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime(timeRemaining);
            }
            else
            {
                timeRemaining = 0;
                timerIsRunning = false;
                Time.timeScale = 0;
                
                Lose();
            }
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void Lose()
    {
        sceneController.FadeOut();
        SceneManager.LoadScene(3);
    }

    public void Win()
    {
        sceneController.FadeOut();
        SceneManager.LoadScene(2);
    }
}
